import { Button, Container, Grid, List, ListItem, TextField } from "@mui/material"
import { useDispatch, useSelector } from "react-redux"

// khai báo action
import { inputChangeHandle, addTaskClikAction, SukienDoiMau} from "../actions/Task.actions"

function Task (){
    // khai báo thư viện useDispath có tác dụng đẩy action vào store
    const dispath = useDispatch()

    // muốn lấy giá trị từ redux sử dụng hàm trong redux là useSelector
    // const state = useSelector((reduxData)=>{
    //     // đầu vào function là reduxData là toàn bộ cái state task.reducer
    //     console.log(reduxData);
    //     return reduxData.taskReducer
    // })

    // để code rõ ràng hơn thì sử dụng phép gán phá hủy cấu trúc đối tượng lấy giá trị 
    const {inputString,tasklist} = useSelector((reduxData)=>{
        return reduxData.taskReducer // lấy cấu trúc redux data
    })

    // kiểm tra giá trị
    //console.log(tasklist)

    // khai báo sự kiên onChange
    const inputTasskChangeHandler=(event)=>{
            dispath(inputChangeHandle(event.target.value))
    }
    // khai báo sự kiện onclik
    const addTasskClickHandler = ()=>{
        // đẩy inputstrig vào task list
        dispath(addTaskClikAction())
    }

    const doimauchu = (index)=>{
        dispath(SukienDoiMau(index));
    }

    return(
        <Container>
            <Grid container mt={5} alignItems="center">
                <Grid item xs={12} md={6} ld={8} sm={12}>
                    <TextField label="Nhập nội dung tại đây" variant="outlined" fullWidth value={inputString} onChange={inputTasskChangeHandler}></TextField>
                </Grid>
                <Grid item xs={12} md={6} lg={4} sm={12} textAlign="center">
                    <Button variant="contained" onClick={addTasskClickHandler}>Thêm task</Button>
                </Grid>
            </Grid>
            <Grid container>
                <List>
                    {tasklist.map((element, index)=>{
                        return <ListItem key={index} onClick={()=>{doimauchu(index)}} style={{color: element.status? "green":"red"}}>
                               {index+1}. {element.taskName}
                        </ListItem>
                    })}
                </List>
            </Grid>
        </Container>

    )
}

export default Task
// improt const để validate
import { TASK_INPUT_CHANGE, TASK_ADD_CLICKED, TASK_CHANGE_COLER} from "../constants/TaskConstants"

// Khỏi tạo hai state cần thay đổi
const initialState = {
    inputString:"",
    tasklist:[]
}
// reducer là một function chứ hai thực thể giá trị khởi tạo reducer bằng giá trị state
const taskReducer =(state = initialState, action)=>{
    // sử dụng hàm swith
    switch (action.type){
        case TASK_INPUT_CHANGE:
            state.inputString=action.payload
            break;
        case TASK_ADD_CLICKED:
            if(state.inputString){
                state.tasklist.push({
                    taskName: state.inputString,
                    status: false
                });
                state.inputString = ""
            }
            break;
        case TASK_CHANGE_COLER:
            state.tasklist[action.payload].status = !state.tasklist[action.payload].status
            break;
        default:
            break;
    }
    console.log(action)
        // tạm thời return state
        return {...state}

}

export default taskReducer
//Root reducer 
import { combineReducers } from "redux";
import taskReducer from "./task.reducer";
// Reducer là một oject
const rootReducer = combineReducers ({
        // improt task reducer qua indexRoot
        taskReducer: taskReducer
});

export default rootReducer;
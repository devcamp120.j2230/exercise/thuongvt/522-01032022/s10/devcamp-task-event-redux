// đây là nơi mô tả sự kiện
import { TASK_INPUT_CHANGE, TASK_ADD_CLICKED, TASK_CHANGE_COLER} from "../constants/TaskConstants";


// nhận đầu vào nhà trả về mô tả
// Nơi mô tả sự kiện liên quan tới task  nơi chứa hàm return về mô tả
const inputChangeHandle =(inputValue)=>{
    return{
        type: TASK_INPUT_CHANGE,
        payload: inputValue
    }
}
// mô tả sự kiện liên quan tới task add
const addTaskClikAction = ()=>{
    return {
        type: TASK_ADD_CLICKED
    }
}

// mo tả sự kiện liên quan tới  đổi màu task
const SukienDoiMau = (index)=>{
    return {
        type: TASK_CHANGE_COLER,
        payload: index
    }
}
export { // improt vào component
    inputChangeHandle,
    addTaskClikAction,
    SukienDoiMau
}
